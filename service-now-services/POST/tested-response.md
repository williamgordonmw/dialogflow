# response

{
  "fulfillmentText": "Incident INC0000050 is currently assigned to Beth Anglin. Current state of the incident is In Progress. This incident was last updated by admin on 2019-07-02 19:49:40. If you want you can ask for the update from Beth Anglin by updating additional comments.",
  "payload": {
    "google": {
      "expectUserResponse": true,
      "richResponse": {
        "items": [
          {
            "simpleResponse": {
              "textToSpeech": "Incident INC0000050 is currently assigned to Beth Anglin. Current state of the incident is In Progress. This incident was last updated by admin on 2019-07-02 19:49:40. If you want you can ask for the update from Beth Anglin by updating additional comments."
            }
          }
        ]
      }
    }
  }
}