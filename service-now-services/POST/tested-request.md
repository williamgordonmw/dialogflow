# request

{
  "responseId": "5ad37fca-34a9-49fa-9c38-8076307e1215-68e175c7",
  "queryResult": {
    "queryText": "please check the state of INC0000050",
    "parameters": {
      "any": "INC0000050"
    },
    "allRequiredParamsPresent": true,
    "fulfillmentMessages": [
      {
        "text": {
          "text": [
            ""
          ]
        }
      }
    ],
    "intent": {
      "name": "projects/testagent-abnoog/agent/intents/aa438533-b0df-463d-bdd0-18c0aabb4a89",
      "displayName": "simpleServiceNow"
    },
    "intentDetectionConfidence": 0.8239843,
    "languageCode": "en"
  },
  "originalDetectIntentRequest": {
    "payload": {}
  },
  "session": "projects/testagent-abnoog/agent/sessions/fde7c70e-2700-3344-f0ca-cf281d46c678"
}
