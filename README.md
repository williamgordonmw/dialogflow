## Manual Testing with Postman

npm install

npm start

https://dashboard.ngrok.com/get-started

./ngrok http 5000

https://9cbb739a.ngrok.io

Before creating and testing dialogflow, always first test first with Postman..

POST https://9cbb739a.ngrok.io/ai
{
    "result": 
    {
        "platform": "servicenow",
        "action": "reportIncident",
        "short_description": "testing from postman to local nodejs to remote servicenow service now.  hope this works!",
        "comments":" i am filled with anticipation.",
        "category": "tech support"
    }
}

Response:
{
    "speech": " Your incident is noted. We will let you know after completing. Please note this Id - INC0010017 for further reference ",
    "displayText": " Your incident is noted. We will let you know after completing. Please note this Id - INC0010017 for further reference ",
    "source": "reportIncidentBot"
}

POST https://9cbb739a.ngrok.io/ai
{
    "result": 
    {
        "platform": "servicenow",
        "action": "getIncident",
        "incidentId":" INC0010017"
    }
}

Response:
{
    "speech": "Reqeuest Confirmation",
    "displayText": "Reqeuest Confirmation",
    "message": {
        "platform": "servicenow",
        "speech": "Hi. I am your ServiceNow Assistant. Responding to your query. "
    },
    "incidentJson": {
        "result": [
            {
                "Number": "INC0010017",
                "State": "New",
                "Short Description": "testing from postman to local nodejs to remote servicenow service now.  hope this works!",
                "Assignment Group": ""
            }
        ]
    }
}


## DialogFlow

Login to DialogFlow dahsboard (https://console.dialogflow.com) 

create agent 


### Start Simple

#### from dilogflow console, create service now simple intent

simpleServiceNow

Training Phrases:

please check the state of NG999

please check number NG999

please check NG999

check NG999

Required Parameters:

any : @sys:any

#### from servicenow console, create service now rest api

from service now (https://dev71283.service-now.com/nav_to.do?uri=%2F$restapi.do%3F)

search and create new scripted rest api 

(note - do a sanity test by searching for you endpoint in servicenow search "REST API Explorer" 377308 / Dialogflow, then copy the request from "diagnostic info" into the raw body and test.)

Then under Dialogflow Rest API, create new scripted REST resource (in service-now-services/POST directory)

https://dev71283.service-now.com/api/377308/dialogflow

#### from dialogflow console, create fulfillment and create webhook

https://dev71283.service-now.com/api/377308/dialogflow

admin / M____3  
content-type application/json

Test with:

"please check the state of INC0000050"


DONE.

---



Now onto the good stuff.  Test against local.

### ServiceNow DialogFlow to create a ticket incident

Create CreateIncidentIntent:
TODO

create the webhook:
https://9cbb739a.ngrok.io/ai

Test, Test, Test


### ServiceNow DialogFlow to retrieve a ticket incident

Create GetIncidentIntent:
TODO

create the webhook:
https://9cbb739a.ngrok.io/ai

Test, Test, Test


---



Now onto the best stuff.  Deploy to Heroku and Test

### ServiceNow DialogFlow to create a ticket incident

Create CreateIncidentIntent:
TODO

create the webhook:
https://heroku.../ai

Test, Test, Test


### ServiceNow DialogFlow to retrieve a ticket incident

Create GetIncidentIntent:
TODO

create the webhook:
https://heroku.../ai

Test, Test, Test


---

## Next steps

A little CI/CD maybe?